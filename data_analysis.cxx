#include <fstream>
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TPad.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TF1.h"
#include "TRandom.h"
#include "iomanip"
#include "TMath.h"
#include <type_traits>
#include "functions.hxx"
#include "TColor.h"
#include "TH1F.h"
#include "TMarker.h"
#include "TAttMarker.h"
#include "TMultiGraph.h"
#include "TVirtualFitter.h"
#include "RooFit.h"
#include "Fit/Fitter.h"
#include <iostream>

//STARTING VALUES
Double_t constexpr par[7] = {.5, 50., 999.205, 0.05676, 0.00000001431, .80063, 8e5};
Double_t constexpr width = 1920 * 3;
Double_t constexpr height = 1080 * 2.6;
int main()
{
    double cmeasures[1];
    double cweights[1];
    double lmeasures[1];
    double lweights[1];
    double r1[1];
    double r1w[1];
    double r2[1];
    double r2w[1];

    ROOT::Fit::Fitter fitter;

    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    ROOT::Math::MinimizerOptions::SetDefaultPrecision(std::numeric_limits<double>::epsilon());
    ROOT::Math::MinimizerOptions::SetDefaultMaxIterations(1e5);
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(2);
    ROOT::Math::MinimizerOptions::SetDefaultMaxFunctionCalls(1e5);
    ROOT::Math::MinimizerOptions opt;

    //opt.SetMinimizerAlgorithm("Minuit");
    //opt.SetMinimizerType("Minuit2");

    opt.Print();
    fitter.Config().SetMinimizerOptions(opt);

    gStyle->SetOptTitle(0);
    gStyle->SetOptStat(10);
    gStyle->SetOptFit(1110);
    gStyle->SetStatY(0.9);
    gStyle->SetStatX(0.9);
    gStyle->SetStatW(1);
    gStyle->SetStatH(1);

    TCanvas *c = new TCanvas();
    TLegend legend(1., 0.95, 0.82, 0.79, "");
    c->SetCanvasSize(width, height);
    c->GetPad(0)->SetLogx();
    c->GetPad(0)->SetGrid();

    //FREQUENCY RESPONSE

    TMarker fitpoint;
    TMarker fitpoint2;
    fitpoint.SetX(1.00025e+02);
    fitpoint2.SetX(0.931323);
    fitpoint.SetMarkerStyle(106);
    fitpoint2.SetMarkerStyle(106);
    fitpoint.SetMarkerSize(10.F);
    fitpoint2.SetMarkerSize(10.F);
    TGraphErrors highresponse("Csigmas.txt", "%lg %lg %*lg %lg %lg");
    TGraphErrors lowresponse("Lsigmas.txt", "%lg %lg %*lg %lg %lg");
    TGraphErrors topvoltage("Isigmas.txt", "%lg %lg %*lg %lg %lg");
    TGraphErrors tPhase("Csigmas.txt", "%lg %*lg %lg %lg %*lg %lg");
    TGraphErrors wPhase("Lsigmas.txt", "%lg %*lg %lg %lg %*lg %lg");
    TGraphErrors deltadata("delta.txt", "%lg %lg %lg %lg");

    {
        Double_t const *pars = par;
        SetParameters(pars);
    }

    //setting graph styles
    //RFgen
    //cmeasures[0] = Vin.GetParameter(4);
    //cweights[0] = 1 / (Vin.GetParError(4)*Vin.GetParError(4));

    //TWITTER
    {
        int i = 0;
        TGraphErrors *g[6] = {&highresponse, &lowresponse, &topvoltage, &tPhase, &wPhase, &deltadata};
        for (auto const a : g)
        {
            a->SetTitle("Frequency Response");
            a->SetLineColor(kBlue);
            a->SetMarkerColor(kBlue);
            a->SetMarkerSize(5.F);
            a->SetLineWidth(5.F);
            a->SetMarkerStyle(1);
            a->GetXaxis()->SetTitle("Frequency (Hz)");
            a->GetXaxis()->SetTitleOffset(.7f);
            a->GetYaxis()->SetTitle("Linear Gain #left(#frac{v}{v_{i}}#right)");

            ++i;
        }
        topvoltage.GetYaxis()->SetTitle("v_{i} (V)");
        topvoltage.GetYaxis()->SetTitleOffset(.75f);
        topvoltage.GetYaxis()->SetTitleSize(0.05);
        topvoltage.GetYaxis()->SetTitleSize(0.05);
    }
    Vt.FixParameter(0, Vin.GetParameter(0));
    Vt.FixParameter(1, Vin.GetParameter(1));
    highresponse.Fit(&gVt, "ME");
    r1[0] = gVt.GetParameter(0);
    r1w[0] = 1 / (gVt.GetParError(0) * gVt.GetParError(0));
    cmeasures[0] = gVt.GetParameter(1);
    cweights[0] = 1 / (gVt.GetParError(1) * gVt.GetParError(1));

    //WOOFER
    Vw.FixParameter(0, Vin.GetParameter(0));
    Vw.FixParameter(1, Vin.GetParameter(1));
    Vw.FixParameter(6, Vt.GetParameter(6));
    Vw.SetParError(6, Vt.GetParError(6));
    lowresponse.Fit(&gVw, "ME");
    r2[0] = gVw.GetParameter(0);
    r2w[0] = 1 / (gVw.GetParError(0) * gVw.GetParError(0));
    lmeasures[0] = gVw.GetParameter(1);
    lweights[0] = 1 / (gVw.GetParError(1) * gVw.GetParError(1));

    highresponse.GetHistogram()->SetMinimum(0);
    gStyle->SetStatY(0.9);
    gStyle->SetStatX(.37);
    gStyle->SetStatW(0.15);
    gStyle->SetStatH(0.2);

    highresponse.Draw("APE");
    fitpoint.SetY(highresponse.GetY()[highresponse.GetN() - 2]);
    fitpoint2.SetY(highresponse.GetY()[highresponse.GetN() - 1]);
    fitpoint.Draw();
    fitpoint2.Draw();
    c->Print("Vt.png");
    delete c;
    c = new TCanvas();

    c->SetCanvasSize(width, height);
    c->GetPad(0)->SetLogx();
    c->GetPad(0)->SetGrid();
    fitpoint.SetY(lowresponse.GetY()[lowresponse.GetN() - 2]);
    fitpoint2.SetY(lowresponse.GetY()[lowresponse.GetN() - 1]);
    lowresponse.Draw("APE");
    fitpoint.Draw();
    fitpoint2.Draw();
    gStyle->SetStatY(0.35);
    c->Print("Vw.png");

    legend.AddEntry(&highresponse, "V_{t}");
    legend.AddEntry(&lowresponse, "V_{w}");
    legend.AddEntry(&Vin, "V_{Rfgen}");
    legend.Draw();

    delete c;
    c = new TCanvas();
    c->SetCanvasSize(width, height);
    TF1 deltagain("Response Delta", &diff, 1e2, 1e4, 6);
    deltagain.SetParameters(gVw.GetParameter(0), gVw.GetParameter(1), gVw.GetParameter(2), gVt.GetParameter(0), gVt.GetParameter(1), gVt.GetParameter(2));
    Diff.SetParameters(gVw.GetParameter(0), gVw.GetParameter(1), gVw.GetParameter(2), gVt.GetParameter(0), gVt.GetParameter(1), gVt.GetParameter(2));

    //deltagain.SetParLimits(1, par[3] - par[3]/2., par[3] + par[3]/2.);
    //deltagain.SetParLimits(4,par[4]-par[4]/2,par[4]+par[4]/2);
    //deltagain.FixParameter(2,gVw.GetParameter(2));
    //deltagain.FixParameter(5,gVw.GetParameter(2));
    deltagain.SetParLimits(0, 970, 1050);
    deltagain.SetParLimits(3, 970, 1050);
    deltadata.Fit(&deltagain, "0 ME"); /*
    cmeasures[1] = deltagain.GetParameter(4);
    cweights[1] = 1 / (deltagain.GetParError(4) * deltagain.GetParError(4));
    lmeasures[1] = deltagain.GetParameter(1);
    lweights[1] = 1 / (deltagain.GetParError(1) * deltagain.GetParError(1));
    r1[1] = deltagain.GetParameter(3);
    r1w[1] = 1 / (deltagain.GetParError(3) * deltagain.GetParError(3));
    r2[1] = deltagain.GetParameter(0);
    r2w[1] = 1 / (deltagain.GetParError(0) * deltagain.GetParError(0));*/
    deltagain.SetRange(6e3, 7e3);
    deltagain.GetYaxis()->SetTitle("|v_{w}-v_{t}| (mV)");
    deltagain.GetXaxis()->SetTitle("Frequency (Hz)");
    deltagain.GetYaxis()->SetNdivisions(20 + 100 * 5 + 10000 * 5);
    deltagain.GetXaxis()->SetNdivisions(10 + 100 * 5 + 10000 * 5);
    deltadata.GetYaxis()->SetTitle("1000#times#frac{|v_{w}-v_{t}|}{v_{i}}");
    deltadata.GetXaxis()->SetTitle("Frequency (Hz)");
    deltadata.GetYaxis()->SetNdivisions(20 + 100 * 5 + 10000 * 5);
    deltadata.GetXaxis()->SetNdivisions(10 + 100 * 5 + 10000 * 5);

    deltagain.GetHistogram()->SetMinimum(-0.5);

    //Double_t minX = deltagain.GetMinimumX(6e3, 7e3, std::numeric_limits<double>::epsilon(), std::numeric_limits<int>::max(), false);
    //Double_t minY = deltagain.Eval(minX);
    //std::cout << "Crossover frequency estimate: " << minX << '\n'
    //          << "Delta at minimum:\t" << minY << '\n';
    TPad *grid = new TPad("grid", "", 0, 0, 1, 1);
    grid->Draw();
    grid->cd();
    grid->SetGrid();
    grid->SetFillStyle(4000);
    TF1 otherdelta("Response Delta", &diff, 5e3, 8e3);

    TMarker minimum;
    minimum.SetMarkerStyle(106);
    minimum.SetMarkerSize(15.F);
    TMarker otherminimum = minimum;
    //minimum.SetX(minX);
    //minimum.SetY(minY);
    Double_t minX = otherdelta.GetMinimumX(6e3, 7e3, std::numeric_limits<double>::epsilon(), std::numeric_limits<int>::max(), false);
    Double_t minY = diff(&minX, nullptr);
    std::cout << "Crossover frequency estimate: " << minX << '\n'
              << "Delta at minimum:\t" << minY << '\n';
    otherminimum.SetX(minX);
    otherminimum.SetY(minY);
    deltadata.SetFillColor(TColor::GetColorTransparent(kAzure - 7, 0.1));
    deltadata.GetHistogram()->SetAxisRange(6400, 6800);
    deltadata.GetHistogram()->SetMaximum(40);
    deltadata.GetHistogram()->SetMinimum(-5);
    deltadata.Draw("A5");
    deltadata.SetTitle("Response Gain Delta");
    deltagain.SetFillColor(kRed);
    otherdelta.SetFillColor(kRed);
    otherdelta.SetLineColor(kRed);
    otherdelta.SetLineWidth(20);
    otherdelta.SetMarkerSize(20);
    deltagain.SetMarkerStyle(0);
    otherdelta.SetMarkerStyle(0);

    //deltagain.Draw("E4 same");
    otherdelta.Draw("E3 same");

    //minimum.Draw();
    otherminimum.Draw();
    gStyle->SetOptFit(0);

    /*
        deltagain.SetParameters(gVw.GetParameter(0),gVw.GetParameter(1),gVw.GetParameter(2),gVt.GetParameter(0),gVt.GetParameter(1),gVt.GetParameter(2));
        minX = deltagain.GetMinimumX(5e3, 8e3, std::numeric_limits<double>::epsilon(), std::numeric_limits<int>::max(), false);
        minY = diff(&minX, nullptr);
        minimum.SetX(minX);
        minimum.SetY(minY);
        minimum.Draw();
        deltagain.Draw("same");*/

    c->Print("delta.png");
    delete c;
    gStyle->SetOptFit(1110);
    c = new TCanvas();
    c->SetCanvasSize(width, height);
    c->GetPad(0)->SetLogx();
    c->GetPad(0)->SetGrid();

    tPhase.GetYaxis()->SetTitle("#phi (degrees)");
    tPhase.GetYaxis()->SetTitleOffset(.7f);
    tPhase.GetXaxis()->SetTitleOffset(.7f);
    tPhase.GetYaxis()->SetTitleSize(0.05);
    tPhase.GetXaxis()->SetTitleSize(0.05);
    wPhase.GetYaxis()->SetTitle("#phi (degrees)");
    /*
        for (int i = 0; i < tPhase.GetN(); ++i)
            tPhase.GetY()[i] *= M_PI / 180;
        for (int i = 0; i < wPhase.GetN(); ++i)
            wPhase.GetY()[i] *= M_PI / 180;*/
    tPhase.GetHistogram()->SetMaximum(90);
    tPhase.GetHistogram()->SetMinimum(-5);
    wPhase.GetHistogram()->SetMinimum(-90);
    wPhase.GetHistogram()->SetMaximum(5);
    TAxis *axis = tPhase.GetYaxis();
    //axis->SetNdivisions(-1004);
    //axis->ChangeLabel(-1, -1, -1, -1, -1, -1, "90");
    //axis->ChangeLabel(-2, -1, -1, -1, -1, -1, "72.5");
    //axis->ChangeLabel(-3, -1, -1, -1, -1, -1, "45");
    //axis->ChangeLabel(-4, -1, -1, -1, -1, -1, "27.5");
    axis = wPhase.GetYaxis();
    //axis->SetNdivisions(-1004); /*
    /*axis->ChangeLabel(1, -1, -1, -1, -1, -1, "-#frac{#pi}{2}");
        axis->ChangeLabel(2, -1, -1, -1, -1, -1, "-#frac{3#pi}{8}");
        axis->ChangeLabel(3, -1, -1, -1, -1, -1, "-#frac{#pi}{4}");
        axis->ChangeLabel(4, -1, -1, -1, -1, -1, "-#frac{#pi}{8}");*/
    tPhase.SetFillColorAlpha(kBlue, 0.5);
    fitpoint.SetY(tPhase.GetY()[tPhase.GetN() - 2]);
    fitpoint2.SetY(tPhase.GetY()[tPhase.GetN() - 1]);
    tPhase.Fit(&PHIt, "ME");
    wPhase.Fit(&PHIw, "ME");
    tPhase.Draw("a5");
    fitpoint.Draw();
    fitpoint2.Draw();
    gStyle->SetStatY(0.9);

    c->Print("PHIt.png");
    delete c;
    c = new TCanvas();
    c->SetCanvasSize(width, height);
    c->GetPad(0)->SetLogx();
    c->GetPad(0)->SetGrid();
    fitpoint.SetY(wPhase.GetY()[wPhase.GetN() - 2]);
    fitpoint2.SetY(wPhase.GetY()[wPhase.GetN() - 1]);
    wPhase.Draw("AP");
    fitpoint.Draw();
    fitpoint2.Draw();
    gStyle->SetStatY(0.25);
    c->Print("PHIw.png");

    //TRANSIENT
    delete c;
    c = new TCanvas();
    c->SetCanvasSize(width, height);
    TMultiGraph transient;
    TGraphErrors viTransient("sigmatransient.txt", "%lg %lg %*lg %*lg %lg %lg");
    TGraphErrors vtTransient("sigmatransient.txt", "%lg %*lg %lg %*lg %lg %*lg %lg");
    TGraphErrors vwTransient("sigmatransient.txt", "%lg %*lg %*lg %lg %lg %*lg %*lg %lg");
    TF1 vi("FGen", &wave, 0, 4, 3);
    TF1 vt("Tweeter", &wave, 0, 4, 3);
    TF1 vw("Woofer", &wave, 0, 4, 3);
    viTransient.SetTitle("FGen");
    viTransient.SetLineColor(kGreen);
    viTransient.SetLineWidth(3);
    vtTransient.SetTitle("Tweeter");
    vtTransient.SetLineColor(kBlue);
    vtTransient.SetLineWidth(3);
    vwTransient.SetTitle("Woofer");
    vwTransient.SetLineColor(kRed);
    vwTransient.SetLineWidth(3);

    //parameters
    double_t tpars[3] = {4.72086e-01, 0.9313225746, 1.55};
    vi.SetParameters(tpars);
    vt.SetParameters(0., 1., 0);
    vw.SetParameters(tpars);
    vw.SetParLimits(1, 0.1, 1.9);
    vi.SetParLimits(0, 0.45, 0.5);
    vw.SetParLimits(0, 0.45, 0.5);
    vi.SetParLimits(2, -M_PI, M_PI);
    vt.SetParLimits(2, -M_PI, M_PI);
    vw.SetParLimits(2, -M_PI, M_PI);
    vt.SetParLimits(0, 0, 0.1);
    //vi.FixParameter(1, 0.9313225746);
    //vt.FixParameter(1, 0.9313225746);
    //vw.FixParameter(1, 0.9313225746);
    //vt.SetParError(1, 7.03139e-06);

    viTransient.Fit(&vi, "0 ME");
    vtTransient.Fit(&vt, "0 ME");
    vwTransient.Fit(&vw, "0 ME");

    transient.SetTitle("Transient at ~100Hz; Time (s); Tension (V)");

    transient.Add(&viTransient, "EL");
    transient.Add(&vtTransient, "EL");
    transient.Add(&vwTransient, "EL");
    transient.Draw("a");

    c->Print("Transient.png");
    delete c;
    c = new TCanvas();
    c->SetCanvasSize(width, height);
    vi.SetFillColor(kGreen);
    vi.SetLineColor(kGreen);
    vt.SetFillColor(kBlue);
    vt.SetLineColor(kBlue);
    vw.SetFillColor(kRed);
    vw.SetLineColor(kGreen);
    vi.Draw();
    vt.Draw("same");
    vw.Draw("same");
    c->Print("TransientFit.png");
    double cSum;
    double cwSum;
    double lSum;
    double lwSum;
    double r1Sum;
    double r1wSum;
    double r2Sum;
    double r2wSum;
    int i = 0;
    for (auto const &c : cmeasures)
    {
        cSum += cweights[i] * c;
        cwSum += cweights[i];
        lSum += lweights[i] * lmeasures[i];
        lwSum += lweights[i];
        r1Sum += r1w[i] * r1[i];
        r1wSum += r1w[i];
        r2Sum += r2w[i] * r2[i];
        r2wSum += r2w[i];
        ++i;
    }
    std::cout << '\n'
              << "R1 = " << r1Sum / r1wSum << " pm " << 1 / sqrt(r1wSum) << '\n'
              << "R2 = " << r2Sum / r2wSum << " pm " << 1 / sqrt(r2wSum) << '\n'
              << "C = " << cSum / cwSum << " pm " << 1 / sqrt(cwSum) << '\n'
              << "L = " << lSum / lwSum << " pm " << 1 / sqrt(lwSum) << '\n'
              << "Rc = " << PHIt.GetParameter(0) / (cSum / cwSum) << " pm " << ((1 / sqrt(cwSum)) / (cSum / cwSum) + PHIt.GetParError(0) / PHIt.GetParameter(0)) * PHIt.GetParameter(0) / (cSum / cwSum) << '\n'
              << "Rl = " << gVw.GetParameter(2) << " pm " << gVw.GetParError(2) << '\n';
    delete c;
    c = new TCanvas();
    c->SetCanvasSize(width, height);
    c->GetPad(0)->SetLogx();
    c->GetPad(0)->SetGrid();
    fitpoint.SetY(topvoltage.GetY()[topvoltage.GetN() - 2]);
    fitpoint2.SetY(topvoltage.GetY()[topvoltage.GetN() - 1]);
    topvoltage.Draw("APE");
    gStyle->SetOptFit(0);
    Vin.SetParameter(0, 0.5);
    Vin.FixParameter(1, 50);
    Vin.FixParameter(2, 999.21);
    Vin.FixParameter(3, gVw.GetParameter(1));
    Vin.FixParameter(4, gVt.GetParameter(1));
    Vin.FixParameter(5, gVw.GetParameter(2));
    Vin.FixParameter(6, gVt.GetParameter(2));
    topvoltage.Fit(&Vin, "ME");
    Vin.Draw("same");
    fitpoint.Draw();
    fitpoint2.Draw();
    c->Print("Vi.png");
    delete c;
    std::ofstream transientresponse("transientresponse.txt");
    transientresponse << vi.GetParameter(1) << '\t' << vi.GetParameter(0) << '\t' << vi.GetParameter(2) << '\t' << vt.GetParameter(0) << '\t' << vt.GetParameter(2) << '\t' << vw.GetParameter(0) << '\t' << vw.GetParameter(2) << '\t' << vi.GetParError(1) << '\t' << vi.GetParError(0) << '\t' << vi.GetParError(2) << '\t' << vt.GetParError(0) << '\t' << vt.GetParError(2) << '\t' << vw.GetParError(0) << '\t' << vw.GetParError(2);
}