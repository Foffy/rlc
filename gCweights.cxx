#include <fstream>
#include <TF1.h>
#include "functions.hxx"
#include <iomanip>
//measure info
Double_t constexpr par[7] = {5., 50., 999., 0.05378, 0.00000001453, 4.6, 1000000};
//constants
constexpr double residualgain = 80.;
constexpr double residualoffset = 40.;
constexpr double Tint = 20.;
constexpr double Text = 20.;
constexpr double noise = 21.;
constexpr double gaintempco = 13.;
constexpr double offsettempco = 34.;
constexpr double range = .5;
///////////////////////////////////////////////////////////////////////////
//AI ACCURACY FORMULA:                                                   //
//gainerror = residualgain + gaintempco * Tint + Text (ppm)              //
//offseterror = residualoffset + offsettempco * Tint + 60 (ppm)          //
//noiseuncertainty = (noise * 3) / 10 (uV)                               //
//accuracy = x * gainerror + range * offseterror + noiseuncertainty (uV) //
///////////////////////////////////////////////////////////////////////////
double getsigma(double const x)
{
	double gainerror = residualgain + gaintempco * Tint + Text;
	double offseterror = residualoffset + offsettempco * Tint + 60.;
	double noiseuncertainty = ((noise * 3) / 10) / 1e6;
	return (x * gainerror / 1e6 + range * offseterror / 1e6 + noiseuncertainty);
}

int main()
{
	double nu = 0;
	//SetParameters(par);
	std::fstream data("transient.txt");
	std::ofstream sigma("sigmatransient.txt");
	sigma << std::setprecision(16);
	while (data.good())
	{
		double xx[6];
		for(auto &x: xx)
		{
			data >> x;
		}
		sigma << xx[0] << '\t' <<
		xx[1] << '\t' <<
		xx[3] << '\t' <<
		xx[5] << '\t' <<
		(xx[0] - nu)*5e-5 <<  '\t' <<
		getsigma(xx[1]) << '\t' <<
		getsigma(xx[3]) << '\t' <<
		getsigma(xx[5]) << '\n';
		nu = xx[0];
		
	}
}
