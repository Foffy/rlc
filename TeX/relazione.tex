 
\input{preamble.tex}
\begin{document}
	\maketitle
	\tableofcontents
	\newpage

		\section{Abstract}
		L'obiettivo di questa esperienza è analizzare la risposta in frequenza e il transiente di un circuito crossover reale sottoposto ad una tensione sinusoidale. Sono stati quindi stimati i parametri significativi del circuito tramite metodo dei fasori e poi confrontati con le misure sperimentali. La previsione teorica della frequenza di crossover \(\tilde{\nu}_{c}^{\mathrm{fit}}=(6.5 \pm 0.4 )\mathrm{kHz}\), stimata a partire dai parametri ottenuti da fit, e la stima ottenuta dalle misure di capacità e induttanza \(\tilde{\nu}_{c}^{\mathrm{mis}} = (5.58 \pm 0.04)\mathrm{kHz}\), non sono compatibili fra loro, e quest'ultima non lo è con la misura ottenuta \(\nu^{*}_{c} = (6.5\pm0.1)\mathrm{kHz}\). Sono state effettuate due stime delle componenti parassite di L e C, \(\mathrm{R_{C}^{fit}} = (0.61\pm0.01)\mathrm{M\Omega}\), \(\mathrm{R_{L}^{fit}} = (0.8\pm0.1)\mathrm{\Omega}\), ottenute tramite fit; \(\mathrm{R_{C}^{0}} = (0.61\pm0.01)\mathrm{M\Omega}\), \(\mathrm{R_{L}^{0}} = (0.80\pm0.03)\mathrm{\Omega}\), ottenute tramite misura della risposta dei rami tweeter e woofer per \(\nu \to 0\), queste risultano compatibili. Si è osservato che i modelli di induttore e condensatore reale sono coerenti con il comportamento dei filtri per \(\nu \to 0\).
		\section{Introduzione}
		
		
	Il circuito RLC crossover si compone di un ramo tweeter (C-R) e un ramo woofer (L-R) posti in parallelo. I due filtri, a cui viene imposto il medesimo segnale \(\mathrm{v_{i}}(t)\), rispondono in modo complementare alle frequenze che lo compongono: il ramo tweeter sopprime i segnali a piccole frequenze, viceversa il woofer blocca segnali ad alte frequenze. Le curve di risposta dei due rami si intersecano alla frequenza di crossover, che per un circuito ideale è data da:
	\begin{equation}
		\nu_{c} =  \frac{1}{2\pi\sqrt{\mathrm{LC}}} =  \frac{1}{2\pi\sqrt{\tau_{\mathrm{RC}}\tau_{\mathrm{RL}}}}
		\label{eq:nuc}
	\end{equation}
	dove \(\tau_{\mathrm{RC}}\),\(\tau_{\mathrm{RL}}\) sono i tempi caratteristici dei circuiti RC e RL serie ideali. Se il circuito fosse ideale, gli sfasamenti dei segnali in uscita dai due rami, dovrebbero essere uguali in modulo e opposti in segno: \(\mathrm{\phi_{t} (\nu_{c}) = - \phi_{w}(\nu_{c})}\). Nella realtà, a causa di elementi parassiti (\(\mathrm{R_{C}}\),\(\mathrm{R_{L}}\)), la frequenza di crossover osservata \(\nu_{c}^{*}\) subisce uno shift verso frequenze maggiori e gli sfasamenti non hanno lo stesso modulo, quindi l'equazione \eqref{eq:nuc} non è esatta, si ha invece che:
	\[
		\nu_{c}^{*} \xrightarrow[\mathrm{R_L} \to 0]{\mathrm{R_C} \to \infty} \nu_{c}
	\]
	\[
	\mathrm{\phi_{t}^{*} (\nu_{c})} \xrightarrow[\mathrm{R_L} \to 0]{\mathrm{R_C} \to \infty} -\mathrm{\phi_{w}^{*} (\nu_{c})}
	\]
	dove \(\mathrm{R_C}\) e \(\mathrm{R_L}\) sono rispettivamente la resistenza in parallelo del condensatore reale e la resistenza in serie dell'induttore reale\cite{perfettiLr}. Gli elementi parassiti possono essere valutati misurando la risposta dei rami per \(\nu\to0\):
	\begin{equation}
		\mathrm{R_{C}} = \frac{\mathrm{R}(|\vec{\mathrm{V}}_{\mathrm{i}}|(0) - |\vec{\mathrm{V}}_{\mathrm{t}}|(0))}{|\vec{\mathrm{V}}_{\mathrm{t}}|(0)}
			\label{eq:rc}
	\end{equation}
\begin{equation}
	\mathrm{R_{L}} = \frac{\mathrm{R}(|\vec{\mathrm{V}}_{\mathrm{i}}|(0) - |\vec{\mathrm{V}}_{\mathrm{w}}|(0))}{|\vec{\mathrm{V}}_{\mathrm{w}}|(0)}
		\label{eq:rl}
\end{equation}
infatti le componenti parassite cambiano la risposta dei filtri nel limite \(\nu \to 0\), come mostrato in \autoref{fig:limnu0}.
\begin{table}[H]
	\begin{center}
		\begin{tabular}{|l|c|c|}
			\hline
			& \textbf{Ideale} & \textbf{Reale} \\
			\hline\hline
			$\lim\limits_{\nu \to 0} |\vec{\mathrm{V}}_{\mathrm{t}}|$  & 0 & $\frac{|\vec{\mathrm{V}}_{\mathrm{i}}|}{1 + \mathrm{R_C / R}}$\\
			\hline
			$\lim\limits_{\nu \to 0} |\vec{\mathrm{V}}_{\mathrm{w}}|$  & $|\vec{\mathrm{V}}_{\mathrm{i}}|$ & $\frac{|\vec{\mathrm{V}}_{\mathrm{i}}|}{1 + \mathrm{R_L / R}}$\\
			\hline
		\end{tabular}
	\end{center}
	\caption{Risposte delle componenti ideali e reali per \(\nu \to 0\)}
	\label{fig:limnu0}
\end{table}	
	Inoltre, mentre lo sfasamento del woofer reale ha il medesimo comportamento (più lento al crescere della frequenza) del woofer ideale, il tweeter reale ha uno sfasamento \(\phi_{\mathrm{t}}^{*}(\nu) \xrightarrow[\nu \to 0]{} 0\) invece quello ideale \(\phi_{\mathrm{t}}(\nu) \xrightarrow[\nu \to 0]{} \frac{\pi}{2}\).

		\section{Apparato Sperimentale e Svolgimento}

					\subsection{Componenti}


		Si sono scelte due resistenze, un condensatore e un induttore. Tramite il multimetro digitale di Elvis II sono stati misurati i valori di resistenza, capacità e induttanza delle componenti utilizzate.  I dati sono stati acquisiti secondo quanto riportato in \autoref{fig:RLC}.
		
		\begin{table}[H]
			\begin{center}
				\begin{tabular}{|l|c|c|}
					\hline
					& \textbf{Valore Nominale} & \textbf{Range di Misura}  \\
					\hline \hline
					 \(\mathrm{R_1 = R_2 = R}\) & $1\mathrm{k\Omega}$  & $0-1\mathrm{k\Omega}$ \\
					 \hline
					$\mathrm{C}$  & $15\mathrm{nF}$ & $5-50\mathrm{nF}$\\
					\hline
					$\mathrm{L}$  & $45\mathrm{mH}$ & $10-100\mathrm{mH}$\\
					\hline
				\end{tabular}
			\end{center}
			\caption{Valori nominali e range di misura delle grandezze acquisite tramite DMM}
			\label{fig:RLC}
		\end{table}
		\subsection{Circuito Crossover}
		\begin{wrapfigure}[21]{R}{0.5\textwidth}
			\vspace{-10pt}
			
			\begin{circuitikz} \draw 
				(0,-2) to[sinusoidal voltage source = \(\mathrm{V_{FGen}}\)] (0,0.5)
				to[R=\(R_{\mathrm{FGen}}\), resistors/scale=0.75] (0,1.5) -- (0,2)
				(-0.5,2) node[anchor=east] {\(\mathrm{v_i}\)} to[short,o-*](0,2)
				-- (0,3)
				to[short, -*](3,3) -- (3,2.5)
				to[C =C](3,1) 
				to[short,-*](3,1) -- (2,1) 
				to [ R=\(R_{\mathrm{C}}\), resistors/scale=0.75](2,2.5)
				to[short, -*](3,2.5) 
				(3,1.5) -- (3,0.5)  
				(2.5,0.25) node[anchor=east] {\(\mathrm{v_t}\)} to[short,o-*](3,0.25) (3,0.5) 
				to[R = \(\mathrm{R_1}\)](3,-2) (3,3) -- (5,3) -- (5,2.5)  to[R=\(R_{L}\), resistors/scale=0.75](5,1.5) 
				to[L = L](5,0.5)  
				(4.5,0.25) node[anchor=east] {\(\mathrm{v_w}\)}  to[short,o-*](5,0.25)
				(5,0.5) to[R = \(\mathrm{R_2}\)](5,-2) to[short, -*](3,-2) to[short, -*](0,-2) node[ground]{} 
				;
			\end{circuitikz}
			\caption{Circuito crossover reale realizzato; le coppie \(\mathrm{R_C || C}\) e \(\mathrm{R_L-L}\) rappresentano i circuiti equivalenti del condensatore e dell'induttore reali\cite{perfettiLr}; \(\mathrm{R_{FGen}}=50\Omega\) è la resistenza interna del generatore di funzione di ELVIS, secondo quanto riportato nelle specifiche\cite{elvisspec}.}
			\label{fig:crossover}
		\end{wrapfigure}
		 Il circuito crossover è stato realizzato sulla breadboard di Elvis II come riportato in \autoref{fig:crossover}. Sono stati utilizzati tre canali AI \{0,1,2\}, impostati nel range \(\pm500 \mathrm{mV}\), i cui poli positivi sono stati collegati rispettivamente ai terminali \(\mathrm{v_{i}, v_{t}, v_{w}}\) e i poli negativi a terra (le misurazioni sono state eseguite in modo differenziale). L'acquisizione e il salvataggio dati sono stati svolti tramite un programma LabVIEW. Per generare il segnale sinusoidale si è utilizzato il generatore di funzione di ELVIS II, impostando l'ampiezza \(\mathrm{V_{FGen} = 500mV}\). Il VI realizzato ha eseguito uno sweep in frequenza da \(\sim0.1-8\mathrm{kHz}\) per step di \(\sim100\mathrm{Hz}\), acquisendo 1 periodo con 20 campioni per ogni frequenza, questo per ciascun canale AI(campionamento multicanale). I valori di ampiezza(tensione), frequenza e fase sono stati ottenuti tramite fit dei segnali acquisiti, associando, a ogni misura di tensione, un peso come descritto nell'\autoref{sec:errors}, \autoref{sec:pesiAI}, e utilizzando come modello una sinusoide:
		 \begin{equation}
		\mathrm{V}\cos(2\pi\nu t + \phi)
		 \end{equation}
		 dove \(\mathrm{V}, \ \nu, \ \phi\) sono i parametri da stimare con le rispettive incertezze. Inoltre per valutare in modo più accurato il comportamento per \(\nu \to 0\), sono stati acquisiti due transienti con un numero di campioni più elevato, secondo quanto riportato in \autoref{fig:nuto0}.
		 \begin{table}[H]
		 	\begin{center}
		 		\begin{tabular}{|r|c|c|c|}
		 			\hline
		 			\textbf{Frequenza Sinusoide (Hz)}& \textbf{Campioni/Periodi} & \textbf{Periodi} & \(\mathbf{V_{FGen} (mV)}\)   \\
		 			\hline \hline
		 			\(0.931\) & $10^3$  & $2$ & 500\\
		 			\hline
		 			$100$  & $10^2$ & 4  & 500 \\
		 			\hline
		 		\end{tabular}
		 	\end{center}
		 	\caption{Parametri di acquisizione dei transienti a bassa frequenza}
		 	\label{fig:nuto0}
		 \end{table}
	 \subsection{Dati Ottenuti}
	 \label{sec:data}
	 I dati acquisiti sono stati poi elaborati. I valori di ampiezza dei due filtri in funzione della frequenza sono stati espressi in termini di guadagno: \(\mathrm{gain(V) = \frac{V}{V_i}}\), dove V è l'ampiezza in uscita dal filtro e \(\mathrm{V_i}\) è l'ampiezza generata. Inoltre le fasi sono state riespresse in termini di sfasamento rispetto al generatore di funzione: \(\phi = \phi^* - \phi_0 \), dove \(\phi^*\) e \(\phi_0\) sono rispettivamente le fasi misurate del filtro e del generatore di funzione. A partire da questi dati sono stati eseguiti fit e stimati i parametri significativi del circuito, secondo quanto discusso nella \autoref{sec:results}. Si è considerato il guadagno dei filtri, e non i valori di ampiezza, poiché lo stesso generatore di funzione ha una risposta in frequenza non costante, a causa della resistenza interna, come mostrato in \autoref{fig:vfgen}.
	 \begin{figure}[H]
	 	\centering
	 	\includegraphics[width=1\linewidth]{media/Vi.png}
	 	\caption{\(\mathrm{v_i}\) in funzione della frequenza, la curva in rosso rappresenta il modulo del fasore(\autoref{sec:fasori}, \autoref{sec:vi}) calcolato con i parametri ottenuti dai fit.}
	 	\label{fig:vfgen}
	 \end{figure}
		 

		\section{Risultati e Discussione}
		\label{sec:results}
		
		Sono state adattate le curve teoriche di risposta \(\left|\frac{\vec{\mathrm{V}}}{\vec{\mathrm{V}}_{\mathrm{i}}}\right|(\nu)\) (\autoref{fig:tamp}, \autoref{fig:wamp}) e \(\phi(\nu)\) (\autoref{fig:tphase}) sui campioni di dati ottenuti secondo quanto discusso nella \autoref{sec:data}. Le funzioni utilizzate sono state ricavate scrivendo le impedenze reali dei due rami, riportate in \autoref{sec:fasori}, e sono le seguenti:
		\begin{equation}
			\left|\frac{\vec{\mathrm{V}}_{\mathrm{t}}}{\vec{\mathrm{V}}_{\mathrm{i}}}\right| = \sqrt{\frac{1 + \left(2\pi\nu\tau_{\mathrm{RC}}^{(\mathrm{p})}\right)^{-2}}{1 + \left(2\pi\nu\tau_{\mathrm{RC}}^{(\mathrm{r})}\right)^{-2}}}
	\label{eq:tgain}	
	\end{equation}
	\begin{equation}
 \phi_{\mathrm{t}} = \arctan\left(2\pi\nu\tau_{\mathrm{RC}}^{(\mathrm{p})}\right) - \arctan\left(2\pi\nu\tau_{\mathrm{RC}}^{(\mathrm{r})}\right)
 \label{eq:tphase}
	\end{equation}
	\begin{equation}
			\left|\frac{\vec{\mathrm{V}}_{\mathrm{w}}}{\vec{\mathrm{V}}_{\mathrm{i}}}\right|=\mathrm{ \frac{\mathrm{R}}{\mathrm{R+R_L}}\frac{1}{\sqrt{1 + \left(2\pi\nu\tau_{\mathrm{RL}}^{\mathrm(r)}\right)^2}}}
			\label{eq:wgain}
	\end{equation}

\begin{figure}[H]

		\begin{subfigure}[H]{0.5\textwidth}
			\centering 
			\includegraphics[width=\textwidth]{media/Vt.png}
			\caption{Valori sperimentali del guadagno del ramo tweeter e relativo fit (equazione \eqref{eq:tgain}), \(\mathrm{P}(\chi^2_o > \chi^2_{\nu}) \simeq 6\cdot10^{-14}\)}
			\label{fig:tamp}
		\end{subfigure}
	\begin{subfigure}[H]{0.5\textwidth}
		\centering 
		\includegraphics[width=\textwidth]{media/Vw.png}
		\caption{Valori sperimentali del guadagno del ramo woofer e relativo fit (equazione \eqref{eq:wgain}), \(\mathrm{P}(\chi^2_o > \chi^2_{\nu}) \simeq 6\cdot10^{-6}\)}
		\label{fig:wamp}
	\end{subfigure}
\begin{subfigure}[H]{0.5\textwidth}
	\centering 
	\includegraphics[width=\textwidth]{media/PHIt.png}
	\caption{Valori sperimentali dello sfasamento del ramo tweeter e relativo fit (equazione \eqref{eq:tphase}), \(P(\chi^2_o > \chi^2_{\nu}) = 1\).}
	\label{fig:tphase}
\end{subfigure}
\begin{subfigure}[H]{0.5\textwidth}
	\centering 
	\includegraphics[width=\textwidth]{media/delta.png}
	\caption{Differenza di guadagno dei filtri, il marcatore che giace sullo 0 rappresenta la misura della frequenza di crossover; le due linee rosse rappresentano l'intervallo di confidenza del 95\% delle funzioni adattate}
	\label{fig:deltaamp}
\end{subfigure}
\caption{Grafici di risposta in frequenza dei filtri. I marcatori ``X" nei grafici (a),(b),(c) rappresentano i punti sperimentali estratti dai transienti acquisiti con maggior numero di campioni}
\label{fig:fit}
\end{figure}

\begin{table}[H]
	\begin{subtable}[H]{0.45\textwidth}
		\centering
		\begin{tabular}{|l|c|c|c|}
			\hline
			&\(\mathbf{DMM} \) & \textbf{FIT} & \(\mathrm{P}(|z_o|>z)\) \\
			\hline\hline
			\(\mathrm{R_1}\) & $(0.9995 \pm 0.0006) \mathrm{k}\Omega$  & \((1.00 \pm0.01 ) \mathrm{k}\Omega\) & 0.88\\
			\hline
			\(\mathrm{R_2}\) & \((0.9990 \pm 0.0006) \mathrm{k}\Omega\) & \((0.99 \pm0.02 ) \mathrm{k}\Omega\) &0.75 \\
			\hline
			$\mathrm{C}$  & $(14.3 \pm 0.1)\mathrm{nF}$ & \((13.8 \pm 0.2)\mathrm{nF}\)& 0.025 \\
			\hline
			$\mathrm{L}$  & $(56.8\pm0.6)\mathrm{mH}$ & $(43\pm5)\mathrm{mH}$ & 0.008\\
			\hline
		\end{tabular}
		\caption{Valori di resistenza, capacità e induttanza ottenuti da multimetro digitale di ELVIS e da fit; è stato inoltre riportato un test di compatibilità con la variabile normale standard}
\label{fig:RLCmis}
	\end{subtable}
	\hfill
	\begin{subtable}[H]{0.45\textwidth}
		\centering
		\begin{tabular}{|l|c|c|}
			\hline
			&\(\nu \to 0 \) & \textbf{FIT} \\
			\hline\hline
			\(\mathrm{R_C}\) & $(0.61 \pm 0.1) \mathrm{M}\Omega$  & \((0.61 \pm0.01 ) \mathrm{M}\Omega\)\\
			\hline
			\(\mathrm{R_L}\) & \((0.80\pm 0.03) \Omega\) & \((0.8 \pm0.1 )\Omega\)\\
			\hline
		\end{tabular}
		\caption{Misure delle resistenze parassite.}
		\label{fig:par}
	\end{subtable}
	\caption{misure ottenute}
	\label{tab:temps}
\end{table}
Per quanto riguarda le ampiezze, sia del woofer che del tweeter, il test del \(\chi^2\) sui fit rivela una discrepanza altamente significativa (\autoref{fig:tamp}, \autoref{fig:wamp}), mentre non vi è evidenza di discrepanza sull'andamento della fase del tweeter, che come ci si aspettava tende a 0 per \(\nu \to 0\). I valori delle resistenze stimati tramite fit sono compatibili con quelli ottenuti da DMM, ciò non è vero per capacità e induttanza (\autoref{fig:RLCmis}). Analizzando la differenza di guadagno tra i due filtri si è ottenuto, tramite una minimizzazione numerica, una misura della frequenza di crossover \(\nu^*_{c} = (6.5\pm0.1)\mathrm{kHz}\), che è compatibile con la previsione (equazione \eqref{eq:nuc}) ottenuta a partire dai parametri del fit \(\tilde{\nu}_{c}^{\mathrm{fit}} = (6.5 \pm 0.4)\mathrm{kHz}\), ma non con quella ottenuta dalle misure del DMM \(\tilde{\nu}_c^{\mathrm{dmm}} = (5.58\pm0.04)\mathrm{kHz}\). Le misure delle resistenze parassite ottenute tramite fit e dall'analisi del comportamento per \(\nu \to 0\) (equazioni \eqref{eq:rc}, \eqref{eq:rl}) risultano compatibili (\autoref{fig:par}).


		
		
		
		\section{Conclusioni}
	Dai fit eseguiti risulta che i modelli utilizzati per condensatore ed induttore reali sono coerenti con il comportamento dei filtri a basse frequenze. Tuttavia: il test del \(\chi^2\) ha rivelato discrepanze altamente significative con i modelli di guadagno utilizzati; dai test di compatibilità tra le stime di C, L e le misure risulta una discrepanza significativa; la stima della frequenza di crossover \(\tilde{\nu}_c^{\mathrm{dmm}}\) non è compatibile con la misura \(\nu_c^*\). Questi risultati potrebbero essere dovuti a errori sistematici non compensati o ad una sottostima delle incertezze, che non è stato possibile stimare correttamente, perché le specifiche di ELVIS II riportano valori di accuratezza validi entro un anno dalla calibrazione, mentre l'ultima calibrazione esterna ed interna della scheda utilizzata risale all'anno 2010. È inoltre possibile che i modelli impiegati non descrivano accuratamente il comportamento del sistema considerato. Non è stato possibile determinare in maniera conclusiva la causa delle discrepanze.
		
	\appendix
		\section{Relazioni Notevoli}
		\label{sec:fasori}
		\subsection{Ramo Tweeter}
		\paragraph{Tweeter Ideale}Nel caso ideale l'impedenza del ramo tweeter si scrive \(\mathbb{Z}_{\mathrm{t}} = \mathrm{R} \left(1 - \frac{j}{2\pi\nu \tau_{\mathrm{RC}}}\right)\)
		\paragraph{Tweeter Reale} Nel caso reale l'impedenza del ramo tweeter si scrive \(\mathbb{Z}_{\mathrm{t}} = \mathrm{R}\left[\frac{j + \left(2\pi\nu\tau_{\mathrm{RC}}^{(\mathrm{r})}\right)^{-1}}{j + \left(2\pi\nu\tau_{\mathrm{RC}}^{(\mathrm{p})}\right)^{-1}}\right] \), dove sono stati definiti i tempi caratteristici del circuito RC reale \(\tau_{\mathrm{RC}}^{(\mathrm{r})} \overset{\mathrm{def}}{:=} \frac{\mathrm{RR_C}}{\mathrm{R + R_C}}\mathrm{C} \) e del circuito RC parassita \(\tau_{\mathrm{RC}}^{(\mathrm{p})} \overset{\mathrm{def}}{:=} \mathrm{R_{C}C}\)
		\subsection{Ramo Woofer}
		\paragraph{Woofer Ideale} Nel caso ideale l'impedenza del ramo woofer si scrive \(\mathbb{Z}_{\mathrm{w}} = \mathrm{R}(j2\pi\nu\tau_{\mathrm{RL}} + 1)\)
	    \paragraph{Woofer Reale}Nel caso reale l'impedenza del ramo woofer si scrive \(\mathbb{Z}_{\mathrm{w}} = (\mathrm{R + R_{L}})(j2\pi\nu\tau_{\mathrm{RL}}^{\mathrm(r)} + 1)\), dove è stato definito il tempo caratteristico del circuito RL reale \(\tau_{\mathrm{RL}}^{\mathrm(r)} \overset{\mathrm{def}}{:=} \frac{\mathrm{L}}{\mathrm{R + R_L}}\)
		
		\subsection{\(\vec{\mathrm{V}}_{\mathrm{i}}\)} 
		\label{sec:vi}
		L'espressione analitica del modulo del fasore di tensione ai capi dei rami, anche se esiste, è estremamente complicata ed è stata ottenuta tramite la relazione \(|\vec{\mathrm{V}}_{\mathrm{i}}| = \mathrm{V_{FGen}}|(1 - \mathbb{Y}(\nu)\mathrm{R}_{RFgen})|\) ma non è riportata; per quanto riguarda la fase \(\phi_{0}(\nu)\), questa è stata posta uguale a 0 sottraendola da tutte le misure effettuate \(\implies \vec{\mathrm{V}}_{\mathrm{i}} = \mathrm{V_{FGen}}|(1 - \mathbb{Y}(\nu)\mathrm{R}_{\mathrm{RFgen}})|\leftrightarrow \mathrm{v_{i}}(t) = \mathrm{V_{FGen}}|(1 - \mathbb{Y}(\nu)\mathrm{R}_{\mathrm{RFgen}})|\cdot \cos(2\pi\nu t)\)
		
		
		
		

\section{Stima Degli Errori}
\label{sec:errors}
	\subsection{Tensione AI}
	La stima degli errori sui valori di tensione acquisiti è stata svolta secondo quanto riportato nella scheda tecnica di ELVIS II alla sezione ``AI Absolute Accuracy Table". L'errore sulla misura è dato da:
	\[\mathrm{\Delta V = V\times \mathrm{(GainError)} + \mathrm{Range}\times\mathrm{(OffsetError)}+\mathrm{NoiseUncertainty}}\]
	\cite{elvisspec}
	\subsubsection{Ampiezza Risposta in Frequenza}
	\label{sec:pesiAI}
Sulle ampiezze di tensione, l'incertezza è stata stimata tramite fit dei transienti, usando come peso su ogni misura \(\frac{1}{\mathrm{\Delta V}^2}\). La differenza di temperatura dall'ultima calibrazione interna ed esterna è stata misurata al momento dell'acquisizione: \(\Delta\mathrm{T_{cal} } =20 ^\circ \mathrm{C}\).
	\subsection{R, L, C (DMM)}
	Le incertezze su resistenza, induttanza e capacità misurate col multimetro sono state stimate in accordo con quanto riportato nella scheda tecnica di ELVIS II alla sezione ``DMM":
	\[\mathrm{\Delta R = R\times\mathrm{ppm_{reading}} + \mathrm{Range}\times \mathrm{ppm_{range}}}\]
	\[\mathrm{\Delta L = \frac{L}{100}}\]
	\[\mathrm{\Delta C = \frac{C}{100}}\]\cite{elvisspec}
	
	\bibliographystyle{plain}
\bibliography{refs}
\end{document}