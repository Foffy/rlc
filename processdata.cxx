#include <fstream>
#include <iomanip>
#include <math.h>
int main()
{
    std::fstream rawdata("source.txt");
    std::ofstream wFsource("Isigmas.txt");
    std::ofstream wCsource("Csigmas.txt");
    std::ofstream wLsource("Lsigmas.txt");
    std::ofstream delta("delta.txt");
    wFsource << std::setprecision(10);
    wCsource << std::setprecision(10);
    wLsource << std::setprecision(10);
    while (1)
    {
        double xx[14];

        for (int i = 0; i != 14; ++i)
        {
            rawdata >> xx[i];
        }
        for (int i = 2; i != 8; i += 2)
        {
            xx[i] = xx[i] * 180 / M_PI;
            xx[i + 7] = xx[i + 7] * 180 / M_PI;
        }
        if (int(xx[2] / 360) != 0)
        {
            int divisions = abs(int(xx[2] / 360));
            xx[2] = xx[2] / (divisions*360);
            xx[9] = xx[9] / (divisions*360);
        }
        if (int(xx[4] / 360) != 0)
        {
            int divisions = abs(int(xx[4] / 360));
            xx[4] = xx[4] / (divisions*360);
            xx[11] = xx[11] / (divisions*360);
        }
        if (int(xx[6] / 360) != 0)
        {
            int divisions = abs(int(xx[6] / 360));
            xx[6] = xx[6] / (divisions*360);
            xx[13] = xx[13] / (divisions*360);
        }
        
        //xx[8]*=0.25;
        xx[4] = xx[4] - xx[2];
        xx[6] = xx[6] - xx[2];
        xx[11] = xx[11] + xx[9];
        xx[13] = xx[13] + xx[9];
        //if (xx[4] < 0 || xx[4] > 90 || xx[6] > 0 || xx[6] < -90)
            //continue;
        wFsource << xx[0] << '\t' << xx[1] << '\t' << 0 << '\t' << xx[7] << '\t' << xx[8] << '\t' << 0 << '\n';
        wCsource << xx[0] << '\t' << xx[3] / xx[1] << '\t' << xx[4] << '\t' << xx[7] << '\t' << (xx[10] / xx[3] + xx[8] / xx[1]) * (xx[3] / xx[1]) << '\t' << xx[11] + xx[9] << '\n';
        wLsource << xx[0] << '\t' << xx[5] / xx[1] << '\t' << xx[6] << '\t' << xx[7] << '\t' << (xx[12] / xx[5] + xx[8] / xx[1]) * (xx[5] / xx[1]) << '\t' << xx[13] + xx[9] << '\n';
        delta << xx[0] << '\t' << 1000*abs((xx[3] - xx[5])/xx[1]) << '\t' << xx[7] << '\t' << ((xx[10] / xx[3] + xx[8] / xx[1]) * (xx[3] / xx[1]) + (xx[12] / xx[5] + xx[8] / xx[1]) * (xx[5] / xx[1]))*1000 << '\n';
        if(!rawdata.good())
            break;
    }
}
