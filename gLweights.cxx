#include <fstream>
#include <TF1.h>
#include "functions.hxx"
//measure info
Double_t constexpr par[7] = {5., 50., 999., 0.05378, 0.00000001453, 4.6, 1000000};
//constants
constexpr double residualgain = 60.;
constexpr double residualoffset = 13.;
constexpr double Tint = 1.;
constexpr double Text = 10.;
constexpr double noise = 32.;
constexpr double gaintempco = 13.;
constexpr double offsettempco = 27.;
constexpr double range = 1.;
///////////////////////////////////////////////////////////////////////////
//AI ACCURACY FORMULA:                                                   //
//gainerror = residualgain + gaintempco * Tint + Text (ppm)              //
//offseterror = residualoffset + offsettempco * Tint + 60 (ppm)          //
//noiseuncertainty = (noise * 3) / 10 (uV)                               //
//accuracy = x * gainerror + range * offseterror + noiseuncertainty (uV) //
///////////////////////////////////////////////////////////////////////////
double getsigma(double x, double vin)
{
	double gainerror = residualgain + gaintempco * Tint + Text;
	double offseterror = residualoffset + offsettempco * Tint + 60.;
	double noiseuncertainty = ((noise * 3) / 10) / 1e6;
	return (x * gainerror / 1e6 + range * offseterror / 1e6 + noiseuncertainty) / vin;
}

int main()
{
	SetParameters(par);
	std::fstream data("wLsource.txt");
	std::ofstream sigma("Lsigmas.txt");
	while (data.good())
	{
		double f;
		double x;
		double p;
		//double e_eq = sqrt((abs(gVw.Derivative(f)) * (f * 0.02) * (abs(gVw.Derivative(f)) * (f * 0.02))) + getsigma(x, 1. /*vin*/) * getsigma(x, 1. /*vin*/));
		//double vin;
		data >> f >> x >> p; //>> vin;
		sigma << f << '\t' << x << '\t' << p << '\t' << f*0.000 << '\t' << 0.01 << '\n';
	}
}
